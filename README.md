A very basic implementation of a policy gradient learner for the
[MiniGrid](https://github.com/maximecb/gym-minigrid) OpenAI Gym
environment.

You will need a Python virtual environment with PyTorch already
installed and working. Then you can clone this repo and (make sure
your PyTorch virtualenv is active):

```
git submodule init
git submodule update
cd gym_minigrid
python setup.py install
cd ..

```

If all goes well, you should then be able to:
```
python main.py
```
and watch the agent play (and hopefully learn). MiniGrid will animate
the episodes, and every 100 episodes the average reward per episode is
printed to the console.

